function cacheFunction(cb) {
    let cache ={};
    function invoke(arg) {
        if (arg in cache) {
            return cache(arg);
        } else {
            cache[arg] = cb(arg);
            return cache[arg];
        }
    }return invoke;
}
module.exports = cacheFunction;