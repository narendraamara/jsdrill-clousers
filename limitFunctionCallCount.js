function limitFunctionCallCount(cb,n) {
    let counter = 0;
    function invoke() {
        if (counter < n) {
            counter++
            cb();
        } else {
            return null;
        }
    }return{
        invoke
    }
}
module.exports= limitFunctionCallCount;